## WARNING

Do not modify the `VendorSection` containers. 

Every container in this `VendorSection` folder, will be overwritten after a `composer update`.

To modify the code of any `VendorSection` container, you should first copy the container to the `AppSection` (or any of your custom sections). _(And don't forget to update the namespace)._
